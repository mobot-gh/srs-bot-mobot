﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using NLog;

namespace RurouniJones.OverlordBot.Discord
{
    public class Client
    {
        public string BotToken { private get; set; }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private DiscordSocketClient _socket;

        private CancellationToken _cancellationToken;

        #region Singleton definition

        private static Client _instance;
        private static CommandHandler _commandHandler;

        private static readonly object Lock = new();

        public static Client Instance 
        {
            get 
            {
                // Check if instance needs to be created to avoid unnecessary lock
                // every time you request an instance of the service
                if (!(_instance is null)) return _instance;
                // Lock thread so only one thread can create the first instance
                lock (Lock)
                {
                    // Check if instance needs to be created
                    // This is to avoid initial initialization by two threads.
                    _instance ??= new Client();
                }

                return _instance;
            }
        }

        #endregion

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            // This is an optional feature so we will not connect to discord unless the client token is present
            if (BotToken.Length != 59 && BotToken.Length != 70 && BotToken.Length != 72)
            {
                Logger.Warn("Discord token not configured (should be 59, 70 or 72 characters), skipping Discord login");
                return;
            }

            _cancellationToken = cancellationToken;
            
            await Connect();
        }

        private async Task Connect()
        {
            Logger.Info("Connecting to Discord");

            var config = new DiscordSocketConfig
            {
                LogLevel = LogSeverity.Debug,
                GatewayIntents = GatewayIntents.AllUnprivileged
            };

            _socket = new DiscordSocketClient(config);
            _socket.Log += Log;
            _socket.Connected += SetActivity;

            var commandService = new CommandService();
            _commandHandler = new CommandHandler(_socket, commandService);

            await _commandHandler.InstallCommandsAsync();
            await _socket.LoginAsync(TokenType.Bot, BotToken);
            await _socket.StartAsync();
            await Task.Delay(Timeout.Infinite);
        }

        private static Task Log(LogMessage msg)
        {
            Logger.Debug(msg);
            return Task.CompletedTask;
        }

        private async Task SetActivity()
        {
            Logger.Info("Setting bot activity");
            await _socket.SetGameAsync("DCS World", null, ActivityType.Watching);
        }

        public async Task LogException(ulong serverId, ulong channelId, Exception exception)
        {
            if (_socket == null) return;

            var message = new StringBuilder().AppendLine()
                .AppendLine("```")
                .AppendLine("Exception raised trying to process transmission")
                .AppendLine(exception.GetType().ToString())
                .AppendLine(exception.Message)
                .AppendLine("```")
                .ToString();

            try
            {
                await _socket.GetGuild(serverId)
                    .GetTextChannel(channelId)
                    .SendMessageAsync(message);
            }
            catch (InvalidOperationException ex)
            {
                Logger.Warn(ex, "Error logging transmission to Discord");
            }
        }

        public async Task LogTransmission(TransmissionLog requestReplyTransmissionLog)
        {
            if(_socket == null) return;

            try
            {
                await _socket.GetGuild(requestReplyTransmissionLog.DiscordServer)
                    .GetTextChannel(requestReplyTransmissionLog.DiscordChannel)
                    .SendMessageAsync(requestReplyTransmissionLog.ToDiscordLog());
            }
            catch (InvalidOperationException ex)
            {
                Logger.Warn(ex, "Error logging transmission to Discord");
            }
        }
    }
}
