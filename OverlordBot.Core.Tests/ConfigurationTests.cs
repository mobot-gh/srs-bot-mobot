﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using YamlDotNet.Serialization;

// ReSharper disable StringLiteralTypo

namespace RurouniJones.OverlordBot.Core.Tests
{
    [TestClass]
    public class ConfigurationTests
    {
        [TestMethod]
        public void LoadSampleConfiguration()
        {
            var yaml = File.ReadAllText("Documentation/Configuration.Sample.yaml");

            var Deserializer = new DeserializerBuilder().Build();
            var serviceConfig = Deserializer.Deserialize<Configuration>(yaml);

            Assert.AreEqual("OverlordBot Hoggit", serviceConfig.Name);
            Assert.AreEqual("telemetry-key", serviceConfig.TelemetryKey);

            var azure = serviceConfig.Azure;
            Assert.AreEqual("westus", azure.Region);

            var speech = azure.Speech;
            Assert.AreEqual("d6434c74b6f046ac33a2b3a3a53e15c1", speech.SubscriptionKey);
            Assert.AreEqual(Guid.Parse("46493c79-e53e-ca83-94bb-80e38cda6342"), speech.EndpointId);

            var languageUnderstanding = azure.LanguageUnderstanding;

            Assert.AreEqual(Guid.Parse("2414f770-d586-4707-8e0b-93ce0738c5bf"), languageUnderstanding.AppId);
            Assert.AreEqual("https://YOUR_ENDPOINT_NAME_HERE.cognitiveservices.azure.com/", languageUnderstanding.Endpoint);
            Assert.AreEqual("bb35c9f539ec4f85abb40f3cd512df16", languageUnderstanding.EndpointKey);

            var discord = serviceConfig.Discord;
            Assert.AreEqual("DiscordToken", discord.Token);
            Assert.IsTrue(discord.CommandsEnabled);

            var gameServers = serviceConfig.GameServers.ToList();
            Assert.AreEqual(3, gameServers.Count);

            var gaw = gameServers.First();
            Assert.AreEqual("Georgia At War", gaw.Name);
            Assert.AreEqual("GAW", gaw.ShortName);

            var rpc = gaw.Rpc;
            Assert.AreEqual("gaw.hoggitworld.com", rpc.Host);
            Assert.AreEqual(50051, rpc.Port);

            var simpleRadio = gaw.SimpleRadio;
            Assert.AreEqual("gaw.hoggitworld.com", simpleRadio.Host);
            Assert.AreEqual(5002, simpleRadio.Port);

            var controllers = gaw.Controllers.ToList();
            Assert.AreEqual(4, controllers.Count);

            var overlord = controllers.First();
            Assert.AreEqual("AWACS North", overlord.Name);
            Assert.AreEqual(136.000, overlord.Frequency);
            Assert.AreEqual("AM", overlord.Modulation);
            Assert.AreEqual(Configuration.ControllerConfiguration.ControllerType.Awacs, overlord.Type);
            Assert.AreEqual("Overlord", overlord.Callsign);
            Assert.AreEqual("en-US-AriaNeural", overlord.Voice);
            Assert.AreEqual("srs-coalition-password", overlord.Password);
            Assert.AreEqual((ulong) 737062362896826109, overlord.DiscordLogChannel);

            var tnn = gameServers.Last();
            Assert.AreEqual("TNN", tnn.ShortName);
        }
    }
}
