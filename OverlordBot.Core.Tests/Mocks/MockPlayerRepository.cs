﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Threading.Tasks;
using RurouniJones.OverlordBot.Datastore;
using RurouniJones.OverlordBot.Datastore.Models;

#pragma warning disable 1998

namespace RurouniJones.OverlordBot.Core.Tests.Mocks
{
    internal class MockPlayerRepository : IPlayerRepository
    {
        public Unit FindByCallSignResult { get; set; } = null;
        public Unit FindByCallsignAndCoalitionResult { get; set; } = null;
        public Unit FindByIdResult { get; set; } = null;

        public async Task<Unit> FindByCallsign(string groupName, int flight, int element)
        {
            return FindByCallSignResult;
        }

        public async Task<Unit> FindByCallsignAndCoalition(string groupName, int flight, int element, int coalition)
        {
            return FindByCallsignAndCoalitionResult;
        }

        public async Task<Unit> FindById(string id)
        {
            return FindByIdResult;
        }
    }
}
