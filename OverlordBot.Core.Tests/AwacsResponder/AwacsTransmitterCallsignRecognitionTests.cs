﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;
using RurouniJones.OverlordBot.Core.Tests.Mocks;

namespace RurouniJones.OverlordBot.Core.Tests.AwacsResponder
{
    /// <summary>
    /// Verify the Responder handles incomplete transmitter callsigns gracefully.
    ///
    /// We are only testing situations where the callsign is not fully recognizable/
    /// Transmissions with a fully recognizable callsign will be handled in happy-path tests
    /// </summary>
    [TestClass]
    public class AwacsTransmitterCallsignRecognitionTests
    {
        [TestMethod]
        public async Task WhenAPlayerCallsAwacsWithPartiallyRecognizableCallsign_ThenReturnsResponse()
        {
            var controller = new Awacs.AwacsResponder(new MockPlayerRepository(), new MockUnitRepository(), new MockAirfieldRepository(), false, null, null, "TEST");

            var player = new ITransmission.Player("Dolt", -1, -1);
            var awacs = new ITransmission.Awacs("Overlord");
            var transmission = new BasicTransmission(null, ITransmission.Intents.RadioCheck, player, awacs);
            
            const string expectedResponse = "dolt x x, overlord, i could not fully recognize your callsign";
            var reply = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, reply.ToSpeech());
        }

        [TestMethod]
        public async Task WhenAPlayerCallsAwacsWithUnrecognizableCallsign_ThenReturnsResponse()
        {
            var controller = new Awacs.AwacsResponder(new MockPlayerRepository(), new MockUnitRepository(), new MockAirfieldRepository(), false, null, null, "TEST");

            var player = new ITransmission.Player(null, -1, -1);
            var awacs = new ITransmission.Awacs("Overlord");
            var transmission = new BasicTransmission(null, ITransmission.Intents.RadioCheck, player, awacs);
            
            const string expectedResponse = "last transmitter, overlord, i could not recognize your callsign";
            var reply = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, reply.ToSpeech());
        }

        [TestMethod]
        public async Task WhenAPlayerCallsAwacsWithNoTransmitter_ThenReturnsResponse()
        {
            var controller = new Awacs.AwacsResponder(new MockPlayerRepository(), new MockUnitRepository(), new MockAirfieldRepository(), false, null, null, "TEST");

            var awacs = new ITransmission.Awacs("Overlord");
            var transmission = new BasicTransmission(null, ITransmission.Intents.RadioCheck, null, awacs);
            
            const string expectedResponse = "last transmitter, overlord, i could not recognize your callsign";
            var reply = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, reply.ToSpeech());
        }
    }
}
