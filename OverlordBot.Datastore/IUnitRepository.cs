﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Collections.Generic;
using System.Threading.Tasks;
using RurouniJones.OverlordBot.Datastore.Models;

namespace RurouniJones.OverlordBot.Datastore
{
    public interface IUnitRepository
    {
        public Task<Unit> FindByCallsignAndCoalition(string groupName, int flight, int element, int coalition);

        public Task<List<Unit>> FindHostileAircraft(Geo.Geometries.Point sourcePosition, int coalition,
            List<string> aircraftCodes, string serverShortName);

        public Task<Unit> FindAwacs(string pilot, string serverShortName);
    }
}