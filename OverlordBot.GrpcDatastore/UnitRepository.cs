﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Geo.Geodesy;
using Geo.Geometries;
using NLog;
using RurouniJones.OverlordBot.Datastore;
using RurouniJones.OverlordBot.Datastore.Models;

namespace RurouniJones.OverlordBot.GrpcDatastore
{
    public class UnitRepository : IUnitRepository
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private static readonly List<string> AwacsCodes = new() { "A-50", "E-2D", "E-3A", "KJ-2000" };

        private readonly DataSource _dataSource;

        public UnitRepository(DataSource dataSource)
        {
            _dataSource = dataSource;
        }

        public async Task<List<Unit>> FindHostileAircraft(Point sourcePosition, int coalition, List<string> aircraftCodes, string serverShortName)
        {
            var opposingCoalition = coalition == 1 ? 2 : 1; // TODO change this to an Enum

            var query = _dataSource.Units.Values.Where(bogey =>
            bogey.Coalition == opposingCoalition &&
            bogey.Speed > 25 &&
            bogey.MilStd2525d.SymbolSet == MilStd2525d.Enums.SymbolSet.Air);

            if (aircraftCodes is { Count: > 0 })
            {
                query = query.Where(bogey => aircraftCodes.Contains(bogey.Name));
            }

            query = query.OrderBy(bogey => bogey.Location.CalculateGreatCircleLine(sourcePosition).Distance);

            return new List<Unit>(query.ToList());
        }

        public async Task<Unit> FindAwacs(string pilot, string serverShortName)
        {
            return _dataSource.Units.Values.FirstOrDefault(awacs =>
            awacs.Pilot.StartsWith(pilot) &&
            AwacsCodes.Contains(awacs.Name) &&
            awacs.MilStd2525d.SymbolSet == MilStd2525d.Enums.SymbolSet.Air);
        }

        public async Task<Unit> FindByCallsignAndCoalition(string groupName, int flight, int element, int coalition)
        {
            var matches = _dataSource.Units.Values.Where(p => PilotOrCallsignMatches(p, groupName, flight, element) &&
                                               p.Coalition == coalition).ToList();
            if (matches.Count > 1)
            {
                foreach (Unit unit in matches)
                    _logger.Warn(unit);
                throw new DuplicateCallsignException(groupName, flight, element);
            }
            return matches.FirstOrDefault();
        }

        private static bool PilotOrCallsignMatches(Unit u, string groupName, int flight, int element)
        {
            return u.Pilot.ToLower().Contains($"{groupName.ToLower()} {flight}-{element}") ||
                   u.Pilot.ToLower().Contains($"{groupName.ToLower()} {flight}{element}") ||
                   u.Pilot.ToLower().Contains($"{groupName.ToLower()}{flight}-{element}") ||
                   u.Pilot.ToLower().Contains($"{groupName.ToLower()}{flight}{element}") ||
                   u.Callsign.ToLower().Contains($"{groupName.ToLower()} {flight}{element}") ||
                   u.Callsign.ToLower().Contains($"{groupName.ToLower()}{flight}{element}") ||
                   u.Callsign.ToLower().Contains($"{groupName.ToLower()} {flight}-{element}") ||
                   u.Callsign.ToLower().Contains($"{groupName.ToLower()}{flight}-{element}");
        }
    }
}
