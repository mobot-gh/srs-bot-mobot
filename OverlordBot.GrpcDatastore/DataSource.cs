﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using Geo.Geometries;
using Grpc.Core;
using Grpc.Net.Client;
using NLog;
using RurouniJones.OverlordBot.Datastore;
using RurouniJones.OverlordBot.Datastore.Models;
using RurouniJones.Dcs.Grpc.V0.Mission;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace RurouniJones.OverlordBot.GrpcDatastore
{
    public class DataSource : IDataSource
    {
        public string Host { get; init;}
        public int Port { get; init;}

        public ConcurrentDictionary<string, Unit> Units { get; private set; }

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly ConcurrentQueue<StreamUnitsResponse> diagnosticUnitStream;

        public DataSource(string host, int port, ConcurrentQueue<StreamUnitsResponse> diagnosticUnitStream)
        {
            Host = host;
            Port = port;
            this.diagnosticUnitStream = diagnosticUnitStream;
        }

        public async Task StreamUnitUpdatesAsync(CancellationToken cancellationToken)
        {
            _logger.Info($"Setting up gRPC channel for {Host}:{Port}");
            var handler = new SocketsHttpHandler
            {
                PooledConnectionIdleTimeout = Timeout.InfiniteTimeSpan,
                KeepAlivePingDelay = TimeSpan.FromSeconds(60),
                KeepAlivePingTimeout = TimeSpan.FromSeconds(30),
                EnableMultipleHttp2Connections = true,
            };

            using var channel = GrpcChannel.ForAddress($"http://{Host}:{Port}", new GrpcChannelOptions
            {
                HttpHandler = handler
            });

            while (!cancellationToken.IsCancellationRequested)
            {
                try { 
                    _logger.Info($"Starting new DCS-gRPC unit stream");

                    Units = new ConcurrentDictionary<string, Unit>();
                    var client = new MissionService.MissionServiceClient(channel);
                    var units = client.StreamUnits(new StreamUnitsRequest()
                    {
                        PollRate = 1,
                        MaxBackoff = 30
                    });

                    while (await units.ResponseStream.MoveNext(cancellationToken))
                    {
                        var unitUpdate = units.ResponseStream.Current;
                        #if DEBUG
                        diagnosticUnitStream.Enqueue(unitUpdate);
                        #endif
                        _logger.Trace(unitUpdate);
                        switch (unitUpdate.UpdateCase)
                        {
                            case StreamUnitsResponse.UpdateOneofCase.None:
                                break;
                            case StreamUnitsResponse.UpdateOneofCase.Unit:
                                var sourceUnit = unitUpdate.Unit;
                                var pilot = sourceUnit.HasPlayerName ? sourceUnit.PlayerName : sourceUnit.Name;
                                var newUnit = new Unit
                                {
                                    Coalition = (int)sourceUnit.Coalition - 1,
                                    Id = sourceUnit.Id.ToString(),
                                    Location = new Point(sourceUnit.Position.Lat, sourceUnit.Position.Lon, sourceUnit.Position.Alt),
                                    Altitude = sourceUnit.Position.Alt,
                                    Heading = (int) sourceUnit.Orientation.Heading,
                                    Group = sourceUnit.Group.Name,
                                    Name = sourceUnit.Type,
                                    Pilot = pilot,
                                    Callsign = sourceUnit.Callsign,
                                    Speed = (int)sourceUnit.Velocity.Speed,
                                    MilStd2525d = new MilStd2525d((int)sourceUnit.Coalition - 1, Encyclopedia.MilStd2525d.GetUnitByDcsCode(sourceUnit.Type)?.MilStdCode)
                                };
                                if(!Units.ContainsKey(newUnit.Id))
                                {
                                    _logger.Debug($"Unit added\t Id: {newUnit.Id}\t Pilot: {newUnit.Pilot}\t Callsign: {newUnit.Callsign}\t Type: {newUnit.Name}");
                                }
                                Units[newUnit.Id] = newUnit;
                                break;
                            case StreamUnitsResponse.UpdateOneofCase.Gone:
                                var id = unitUpdate.Gone.Id.ToString();

                                if (Units[id] == null)
                                {
                                    _logger.Warn($"Unit Remove attempted but not found in existing Units: Id {id}");
                                } else
                                {
                                    var goneUnit = Units[id];
                                    _logger.Debug($"Unit removed\t Id: {goneUnit.Id}\t Pilot: {goneUnit.Pilot}\t Callsign: {goneUnit.Callsign}\t Type: {goneUnit.Name}");
                                }
                                Units.Remove(id, out var _);
                                break;
                            default:
                                throw new ArgumentOutOfRangeException($"Unknown unit stream event: {unitUpdate.UpdateCase}");
                        }
                    }
                } catch(TaskCanceledException) {
                    _logger.Info($"Stopped DCS-gRPC unit stream");
                    return;
                }
                catch(RpcException)
                {
                    _logger.Info($"Disconnected from DCS-gRPC unit stream");
                    await Task.Delay(TimeSpan.FromSeconds(10), cancellationToken);
                    continue;
                }
            }
        }
    }
}
