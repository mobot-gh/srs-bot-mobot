﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Text;

namespace RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions
{
    public class SetWarningRadiusTransmission : ITransmission
    {
        public string Text { get; }
        public ITransmission.Intents Intent  { get; }
        public ITransmission.ITransmitter Transmitter  { get; }
        public ITransmission.IReceiver Receiver { get; }
        public int Distance { get; }
        public DateTime ReceivedAt { get; }

        public SetWarningRadiusTransmission(string request, ITransmission.Intents intent, ITransmission.ITransmitter transmitter, ITransmission.IReceiver receiver, int distance)
        {
            Text = request;
            Intent = intent;
            Transmitter = transmitter;
            Receiver = receiver;

            Distance = distance switch
            {
                // People say "Set warning to 30 miles" 
                >= 200 and < 300 => distance - 200,
                // People say "Set warning for 40 miles"
                >= 400 and < 500 => distance - 500,
                _ => distance
            };

            ReceivedAt = DateTime.Now;
        }

        public override string ToString()
        {
            return $"Transmission: {Intent}, Request: {Text}, Transmitter: {Transmitter}, Receiver: {Receiver}, Distance: {Distance}";
        }

        public string ToDiscordLog()
        {
            var transmitterType = "";
            var groupName = "_Unknown_";
            var flight = "_Unknown_";
            var element = "_Unknown_";

            if (Transmitter is ITransmission.Player transmitter)
            {
                transmitterType = transmitter.GetType().Name;
                if (transmitter.GroupName != null) groupName = transmitter.GroupName;
                if (transmitter.Flight >= 0) flight = transmitter.Flight.ToString();
                if (transmitter.Element >= 0) element = transmitter.Element.ToString();
            }

            var sb = new StringBuilder()
                .AppendLine("Request:")
                .AppendLine($"\tText: {Text}")
                .AppendLine($"\tTransmitter: {transmitterType}")
                .AppendLine($"\t\tGroup Name: {groupName}")
                .AppendLine($"\t\tFlight Number: {flight}")
                .AppendLine($"\t\tElement Number: {element}")
                .AppendLine($"\tReceiver: {Receiver?.GetType().Name ?? "_Unknown_"}")
                .AppendLine($"\t\tCallsign: {Receiver?.Callsign ?? "_Unknown_"}")
                .AppendLine($"\tIntent: {Intent}")
                .AppendLine($"\tDistance: {Distance}");

            return sb.ToString();
        }
    }
}
