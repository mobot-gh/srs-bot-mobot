﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Generic;

namespace RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions
{
    public interface ITransmission
    {
        enum Intents
        {
            None,
            RadioCheck,
            BogeyDope,
            LocationOfEntity,
            SetWarningRadius,
            ReadyToTaxi,
            InboundToAirfield,
            CancelWarningRadius,
            Picture,
            Declare,
            AbortInbound,
            MissionAssignment,
            MissionJoin
        }

        public enum MissionType
        {
            Unknown,
            CAP,
            CAS,
            SEAD,
            DEAD,
            BAI,
            Strike,
            Recon,
            ASuW,
            OCA
        }


        public string ToDiscordLog();

        // ReSharper disable once UnusedMemberInSuper.Global
        public string Text { get; }
        public DateTime ReceivedAt { get; }
        public Intents Intent { get; }
        public ITransmitter Transmitter { get; }
        public IReceiver Receiver { get; }

        interface IReceiver
        {
            public string Callsign { get; }
        }

        interface ITransmitter
        {
            // ReSharper disable once UnusedMemberInSuper.Global
            public string Callsign { get; }
        }

        interface ISubject
        {
            // ReSharper disable once UnusedMemberInSuper.Global
            public string Callsign { get; }
        }

        class Commander : IReceiver
        {
            public string Callsign { get; }

            public Commander(string callsign)
            {
                Callsign = callsign.ToLower();
            }

            public override string ToString()
            {
                return Callsign;
            }
        }


        interface IQualifier { }

        record UnknownEntity : ITransmitter, IReceiver, ISubject
        {
            public string Callsign => "Unknown";
        }

        record Player : ITransmitter, ISubject, IReceiver
        {
            public string GroupName { get; }
            public int Flight { get; }
            public int Element { get; }
            public string Callsign { get; }
            
            public Player(string groupName, int flight, int element)
            {
                GroupName = groupName?.ToLower();
                Flight = flight;
                Element = element;
                Callsign = $"{GroupName} {Flight} {Element}";
            }

            public bool IsUnrecognizable()
            {
                return GroupName == null && Flight < 0 && Element < 0;
            }

            public bool IsFullyRecognizable()
            {
                return GroupName != null && Flight >= 0 && Element >= 0;
            }

            public string SpokenWithAmbiguity()
            {
                var groupName = GroupName ?? "x";
                var flight = Flight >= 0 ? Flight.ToString() : "x";
                var element = Element >= 0 ? Element.ToString() : "x";

                return $"{groupName} {flight} {element}";
            }
        }

        record Awacs : IReceiver
        {
            public string Callsign { get; }

            public Awacs(string callsign)
            {
                Callsign = callsign.ToLower();
            }
        }

        record Tanker : ISubject
        {
            public string Callsign { get; }

            public Tanker(string callsign)
            {
                Callsign = callsign.ToLower();
            }
        }

        record Airfield : IReceiver, ISubject
        {
            public string Name { get; }
            public string Controller { get; }

            public string Callsign { get; }

            public Airfield(string name, string controller)
            {
                Name = name;
                Controller = controller?.ToLower();
                Callsign = string.Join(' ', new List<string> {Name, Controller});
            }
        }

        record Aircraft: IQualifier
        {
            public string Identifier;
            public ISet<string> Attributes;

            public Aircraft(string identifier, ISet<string> attributes)
            {
                Identifier = identifier?.ToLower();
                Attributes = attributes ?? new HashSet<string>();
            }
        }
    }
}
