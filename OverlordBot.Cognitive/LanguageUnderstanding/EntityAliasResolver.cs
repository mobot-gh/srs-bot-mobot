﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Collections.Generic;
using System.IO;
using NLog;
using YamlDotNet.Serialization;

namespace RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding
{
    internal class EntityAliasResolver
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly IDeserializer Deserializer = new DeserializerBuilder().Build();

        private static readonly Dictionary<string, string> AirfieldAliases = GenerateAliases("Data/Aliases/Airfields.yaml");
        private static readonly Dictionary<string, string> GroupNameAlias = GenerateAliases("Data/Aliases/Callsigns.yaml");
        private static readonly Dictionary<string, string> UnitAttributes = GenerateAliases("Data/Aliases/UnitAttributes.yaml");


        private static Dictionary<string, string>  GenerateAliases(string filePath)
        {

            var aliasDictionary = Deserializer.Deserialize<Dictionary<string, List<string>>>(File.ReadAllText(filePath));

            var result = new Dictionary<string, string>();

            foreach (var (canonicalName, aliases) in aliasDictionary)
            {
                foreach (var alias in aliases)
                {
                    result.Add(alias, canonicalName);
                }
            }

            return result;
        }

        public static string ResolveAirfield(string airfieldAlias)
        {
            if (airfieldAlias == null) return null;
            airfieldAlias = airfieldAlias.ToLower();
            if (!AirfieldAliases.ContainsKey(airfieldAlias)) return airfieldAlias;
            var canonicalName = AirfieldAliases[airfieldAlias];
            Logger.Debug($"Resolving airfield from {airfieldAlias} to {canonicalName}");
            return canonicalName;
        }

        public static string ResolveGroupName(string groupNameAlias)
        {
            if (groupNameAlias == null) return null;
            groupNameAlias = groupNameAlias.ToLower();
            if (!GroupNameAlias.ContainsKey(groupNameAlias)) return groupNameAlias;
            var canonicalName = GroupNameAlias[groupNameAlias];
            Logger.Debug($"Resolving callsign group name from {groupNameAlias} to {canonicalName}");
            return canonicalName;
        }

        public static string ResolveAircraftAttribute(string attributeAlias)
        {
            if (attributeAlias == null) return null;
            attributeAlias = attributeAlias.ToLower();
            if (!UnitAttributes.ContainsKey(attributeAlias)) return attributeAlias;
            var canonicalName = UnitAttributes[attributeAlias];
            Logger.Debug($"Resolving unit attribute from {attributeAlias} to {canonicalName}");
            return canonicalName;
        }
    }
}
