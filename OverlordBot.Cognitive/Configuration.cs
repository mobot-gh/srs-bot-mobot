﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using Microsoft.CognitiveServices.Speech;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding;
using RurouniJones.OverlordBot.Cognitive.SpeechOutput;
using RurouniJones.OverlordBot.Cognitive.SpeechRecognition;

namespace RurouniJones.OverlordBot.Cognitive
{
    public class Configuration
    {
        public static void ConfigureSpeech(string region, string subscriptionKey, Guid endpointId)
        {
            var speechConfig =  SpeechConfig.FromSubscription(subscriptionKey, region);
            speechConfig.EndpointId = endpointId.ToString();

            TransmissionRecognizer.SpeechConfig = speechConfig;
            TransmissionSynthesizer.SpeechConfig = speechConfig;
        }

        public static void ConfigureLanguageUnderstanding(Guid applicationId, string endpoint, string apiKey)
        {
            TransmissionParser.ApplicationId = applicationId;
            TransmissionParser.Endpoint = endpoint;
            TransmissionParser.ApiKey = apiKey;
        }
    }
}
