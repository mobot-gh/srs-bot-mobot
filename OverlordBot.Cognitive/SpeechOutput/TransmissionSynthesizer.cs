﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Collections.Concurrent;
using System.Threading.Tasks;
using Microsoft.CognitiveServices.Speech;
using NLog;

namespace RurouniJones.OverlordBot.Cognitive.SpeechOutput
{
    public class TransmissionSynthesizer
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        #region Singleton definition

        private static TransmissionSynthesizer _instance;

        private static readonly object Lock = new();

        public static TransmissionSynthesizer Instance 
        {
            get 
            {
                // Check if instance needs to be created to avoid unnecessary lock
                // every time you request an instance of the service
                if (!(_instance is null)) return _instance;
                // Lock thread so only one thread can create the first instance
                lock (Lock)
                {
                    // Check if instance needs to be created
                    // This is to avoid initial initialization by two threads.
                    _instance ??= new TransmissionSynthesizer();
                }

                return _instance;
            }
        }

        #endregion

        private static readonly ConcurrentStack<SpeechSynthesizer> SynthesizerStack = new();

        public static SpeechConfig SpeechConfig { private get; set; }

        private TransmissionSynthesizer() { }

        public async Task<Result> Synthesize(string ssml)
        {
            SpeechSynthesizer synthesizer = null;
            try
            {
                synthesizer = SynthesizerStack.TryPop(out var item)
                    ? item
                    : new SpeechSynthesizer(SpeechConfig, null);

                var speechSynthesisResult = await SynthesizeWithSynthesizer(ssml, synthesizer);

                // ReSharper disable once SwitchStatementHandlesSomeKnownEnumValuesWithDefault
                switch (speechSynthesisResult.Reason)
                {
                    case ResultReason.SynthesizingAudioCompleted:
                        return new Result(speechSynthesisResult);
                    case ResultReason.Canceled:
                    {
                        var cancellation =
                            SpeechSynthesisCancellationDetails.FromResult(speechSynthesisResult);
                        _logger.Warn("Speech Synthesis cancelled");
                        _logger.Warn($"Reason: {cancellation.Reason}");
                        _logger.Warn($"ErrorCode: {cancellation.ErrorCode}");
                        _logger.Warn($"ErrorDetails: [{cancellation.ErrorDetails}]");
                        // Synthesizers will quite often fail if they are not used for a while.
                        // However it is still faster to instantly try again with a second attempt rather than
                        // instantiate a new synthesizer.
                        var result = new Result(await SynthesizeWithSynthesizer(ssml, synthesizer));
                        _logger.Info($"Audio synthesis {result.IsSuccess}, {result.AudioData?.Length} bytes");
                        return result;
                    }
                    default:
                        _logger.Warn($"Unexpected Speech Synthesis Result {speechSynthesisResult.Reason}");
                        return null;
                }
            }
            finally
            {
                SynthesizerStack.Push(synthesizer);
            }
        }

        private static async Task<SpeechSynthesisResult> SynthesizeWithSynthesizer(string ssml, SpeechSynthesizer synthesizer)
        {
            return await synthesizer.SpeakSsmlAsync(ssml);
        }

        public class Result
        {
            public byte[] AudioData { get; }

            public bool IsSuccess { get; }

            public Result(SpeechSynthesisResult speechSynthesisResult)
            {
                AudioData = speechSynthesisResult.AudioData;
                IsSuccess = speechSynthesisResult.Reason == ResultReason.SynthesizingAudioCompleted;
            }
        }
    }
}
