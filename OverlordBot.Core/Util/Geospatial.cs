﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using Geo.Abstractions.Interfaces;
using Geo.Geodesy;
using Geo.Geomagnetism;
using Geo.Geometries;
using NLog;

namespace RurouniJones.OverlordBot.Core.Util
{
    internal class Geospatial
    {
        private const double EarthRadius = 6378137.0;
        private const double DegreesToRadians = 0.0174532925;
        private const double RadiansToDegrees = 57.2957795;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        ///     Calculates the new-point from a given source at a given range (meters) and bearing (degrees).
        ///     Taken from https://adhamhurani.blogspot.com/2010/08/c-how-to-add-distance-and-calculate-new.html
        /// </summary>
        /// <param name="source">Original Point</param>
        /// <param name="range">Range in meters</param>
        /// <param name="trueBearing">Bearing in degrees (Must be true and not magnetic)</param>
        /// <returns>End-point from the source given the desired range and bearing.</returns>
        public static Point CalculatePointFromSource(Point source, double range, double trueBearing)
        {
            var latA = source.Coordinate.Latitude * DegreesToRadians;
            var lonA = source.Coordinate.Longitude * DegreesToRadians;
            var angularDistance = range / EarthRadius;
            var trueCourse = trueBearing * DegreesToRadians;

            var lat = Math.Asin(Math.Sin(latA) * Math.Cos(angularDistance) +
                                Math.Cos(latA) * Math.Sin(angularDistance) * Math.Cos(trueCourse));

            // ReSharper disable once IdentifierTypo
            var dlon = Math.Atan2(Math.Sin(trueCourse) * Math.Sin(angularDistance) * Math.Cos(latA),
                Math.Cos(angularDistance) - Math.Sin(latA) * Math.Sin(lat));
            var lon = (lonA + dlon + Math.PI) % (Math.PI * 2) - Math.PI;

            return new Point(lat * RadiansToDegrees, lon * RadiansToDegrees);
        }

        public static double DegreeToRadian(double angle)
        {
            return Math.PI * angle / 180.0;
        }

        public static double RadianToDegree(double angle)
        {
            return 180.0 * angle / Math.PI;
        }

        public static double TrueToMagnetic(Point position, double trueBearing)
        {
            var magneticBearing = trueBearing - CalculateOffset(position);

            if (magneticBearing < 0) magneticBearing += 360;
            Logger.Trace($"True Bearing: {trueBearing}, Magnetic Bearing {magneticBearing}");
            return magneticBearing;
        }

        private static double CalculateOffset(IPosition position)
        {
            var calculator = new WmmGeomagnetismCalculator(Spheroid.Wgs84);
            var result = calculator.TryCalculate(position, DateTime.UtcNow);
            return result.Declination;
        }
    }
}