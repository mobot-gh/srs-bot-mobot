﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;
using RurouniJones.OverlordBot.Core.Awacs.IntentHandlers;
using RurouniJones.OverlordBot.Core.Monitors;
using RurouniJones.OverlordBot.Core.Responders;
using RurouniJones.OverlordBot.Datastore;
using RurouniJones.OverlordBot.Datastore.Models;

namespace RurouniJones.OverlordBot.Core.Awacs
{
    public class AwacsMonitor : IMonitor
    {
        private readonly IPlayerRepository _playerRepository;
        private readonly IUnitRepository _unitRepository;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly ConcurrentDictionary<string, MonitoredPlayerDetails> _playersMonitored = new();

        public RadioController.QueueTransmissionDelegate QueueTransmission { get; init; }
        public RadioController.LogTransmissionDelegate LogTransmission { get; init; }
        public string AwacsPilot { get; init; }
        public bool LosIsRestricted { get; init; }
        public string ServerShortName { get; init; }

        public AwacsMonitor(IPlayerRepository playerRepository, IUnitRepository unitRepository,
            RadioController.QueueTransmissionDelegate queueTransmissionDelegate,
            RadioController.LogTransmissionDelegate logTransmissionDelegate,
            bool restrictedLineOfSight,
            string awacsPilot,
            string serverShortName)
        {
            _playerRepository = playerRepository;
            _unitRepository = unitRepository;
            QueueTransmission = queueTransmissionDelegate;
            LogTransmission = logTransmissionDelegate;
            LosIsRestricted = restrictedLineOfSight;
            ServerShortName = serverShortName;
            AwacsPilot = awacsPilot;
        }

        public async Task ProcessTransmission(ITransmission transmission)
        {
            switch (transmission.Intent)
            {
                case ITransmission.Intents.SetWarningRadius:
                    await SetWarningRadius((SetWarningRadiusTransmission) transmission);
                    break;
                case ITransmission.Intents.CancelWarningRadius:
                    await CancelWarningRadius(transmission);
                    break;
            }
        }

        public async Task StartMonitoring(CancellationToken cancellationToken)
        {
            _logger.Info("Starting AWACS Monitoring");
            while (!cancellationToken.IsCancellationRequested)
            {
                _logger.Trace("Checking for monitored pilots");
                try
                {
                    await Task.Delay(TimeSpan.FromSeconds(1), CancellationToken.None);
                    foreach (var (id, player) in _playersMonitored) { 
                        try {
                            _logger.Trace($"Checking for {id} - {player.PlayerCallsign}");
                            var reply = await CheckWarningRadius(player);
                            if (reply == null) continue;

                            reply.CallerCallsign = player.PlayerCallsign.SpokenWithAmbiguity();
                            reply.ContinueProcessing = true;

                            QueueTransmission(reply);
                            await LogTransmission(reply.ToSpeech());
                        }
                        catch (Exception ex)
                        {
                            _logger.Warn(ex, $"Error checking Warning for {id} - {player.PlayerCallsign}");
                            continue;
                        }
                    }
                }               
                catch (Exception ex)
                {
                    _logger.Warn(ex);
                    continue;
                }
            }
            _logger.Info("Stopped AWACS Monitoring");
        }

        private async Task SetWarningRadius(SetWarningRadiusTransmission warningRadiusTransmission)
        {
            var transmittingPlayer = (ITransmission.Player) warningRadiusTransmission.Transmitter;

            var transmittingUnit = await _playerRepository.FindByCallsign(transmittingPlayer.GroupName,
                transmittingPlayer.Flight, transmittingPlayer.Element);

            var monitoredPlayerDetails = new MonitoredPlayerDetails(transmittingPlayer, transmittingUnit,
                warningRadiusTransmission.Distance);

            if (_playersMonitored.Keys.Contains(monitoredPlayerDetails.PlayerUnit.Id))
            {
                _logger.Debug($"Player {monitoredPlayerDetails.PlayerCallsign} already in AWACS monitoring. Removed");
                _playersMonitored.Remove(monitoredPlayerDetails.PlayerUnit.Id, out _);
            }

            _logger.Debug($"Player {monitoredPlayerDetails.PlayerCallsign} added to AWACS monitoring");
            _playersMonitored[monitoredPlayerDetails.PlayerUnit.Id] = monitoredPlayerDetails;
        }

        private async Task CancelWarningRadius(ITransmission cancelWarningRadiusTransmission)
        {
            var transmittingPlayer = (ITransmission.Player) cancelWarningRadiusTransmission.Transmitter;

            var transmittingUnit = await _playerRepository.FindByCallsign(transmittingPlayer.GroupName,
                transmittingPlayer.Flight,
                transmittingPlayer.Element);

            _logger.Debug($"Player {transmittingPlayer.Callsign} removed from AWACS monitoring by request");
            _playersMonitored.Remove(transmittingUnit.Id, out _);
        }

        public async Task<Reply> CheckWarningRadius(MonitoredPlayerDetails player)
        {
            
            if (player == null) return null;

            Unit awacs = null;
            if (!string.IsNullOrEmpty(AwacsPilot))
            {
                awacs = await _unitRepository.FindAwacs(AwacsPilot, ServerShortName);
            }

            var result = await new WarningRadiusChecker(_playerRepository, _unitRepository).Process(player.PlayerUnit,
                player.WarningDistance, player.MergedContactsReportedToPlayer, player.BoundaryContactsReportedToPlayer, awacs, ServerShortName, LosIsRestricted);

            // Player no longer exists
            if (result.Deleted)
            {
                _logger.Debug($"Player {player.PlayerCallsign} removed from AWACS monitoring");
                _playersMonitored.Remove(player.PlayerUnit.Id, out _);
                return null;
            }
            player.MergedContactsReportedToPlayer.AddRange(result.NewMergedContacts);
            player.BoundaryContactsReportedToPlayer.AddRange(result.NewMergedContacts);
            player.BoundaryContactsReportedToPlayer.AddRange(result.NewBoundaryContacts);
            return result.Reply;
        }

        public class MonitoredPlayerDetails
        {
            public readonly List<Unit> BoundaryContactsReportedToPlayer = new();
            public readonly List<Unit> MergedContactsReportedToPlayer = new();
            public readonly ITransmission.Player PlayerCallsign;
            public readonly Unit PlayerUnit;
            public readonly int WarningDistance;

            public MonitoredPlayerDetails(ITransmission.Player playerCallsign, Unit playerUnit, int distance)
            {
                PlayerCallsign = playerCallsign;
                PlayerUnit = playerUnit;
                WarningDistance = distance;
            }

            protected bool Equals(MonitoredPlayerDetails other)
            {
                return Equals(PlayerUnit, other.PlayerUnit);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                return obj.GetType() == GetType() && Equals((MonitoredPlayerDetails) obj);
            }

            public override int GetHashCode()
            {
                return PlayerUnit != null ? PlayerUnit.GetHashCode() : 0;
            }
        }
    }
}
