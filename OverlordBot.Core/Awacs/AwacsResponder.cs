﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using NLog;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;
using RurouniJones.OverlordBot.Core.Awacs.IntentHandlers;
using RurouniJones.OverlordBot.Core.Responders;
using RurouniJones.OverlordBot.Datastore;
using RurouniJones.OverlordBot.Datastore.Models;

[assembly: InternalsVisibleTo("OverlordBot.Core.Tests")]
namespace RurouniJones.OverlordBot.Core.Awacs
{
    public class AwacsResponder : BaseResponder
    {
        private const string AwacsOnlyMessage = "this is an AYWACS frequency and we only respond to AYWACS calls";
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly string _callsign;
        public string Callsign
        {
            get => _callsign;
            init => _callsign = value?.ToLower();
        }

        public string AwacsPilot  { get; init; }
        public bool LosIsRestricted { get; init; } = false;
        public string ServerShortName  { get; init; }

        public AwacsResponder(IPlayerRepository playerRepository, IUnitRepository unitRepository, IAirfieldRepository airfieldRepository,
            bool restrictedLineOfSight, string awacsPilot, string awacsCallsign, string serverShortName)
                : base(playerRepository, unitRepository, airfieldRepository) {
            LosIsRestricted = restrictedLineOfSight;
            ServerShortName = serverShortName;
            AwacsPilot = awacsPilot;
            Callsign = (awacsCallsign?.Length ?? 0) > 0 ? awacsCallsign : null;
        }

        public override async Task<Reply> ProcessTransmission(ITransmission transmission)
        {
            _logger.Info(transmission);

            var reply = PerformInitialTransmissionChecks(transmission);
            if (reply.ContinueProcessing == false) return reply;

            if (!IsAddressedToAwacs(transmission.Receiver))
            {
                var typeName = transmission.Receiver.GetType().Name;
                var article = new List<char> { 'a', 'e', 'i', 'o'}.Contains(typeName.ToLower()[0]) ? "an" : "a";
                reply.Notes = $"Transmission was not addressed to an AWACS. It was addressed to \"{transmission.Receiver.Callsign}\" which is {article} {typeName}";
                reply.ContinueProcessing = false;
                reply.Message = null;

                _logger.Info(reply.Notes);
                return reply;
            }

            if (!IsAddressedToController(transmission.Receiver))
            {
                reply.Notes =
                    $"Transmission was not addressed to \"{Callsign ?? "overlord"}\". Receiver callsign was \"{transmission.Receiver?.Callsign ?? "null"}\""; 
                reply.ContinueProcessing = false;
                reply.Message = null;

                _logger.Info(reply.Notes);
                return reply;
            }

            reply.ResponderCallsign = GetResponseCallsign(transmission.Receiver);

            Unit awacs = null;
            if (!string.IsNullOrEmpty(AwacsPilot))
            {
                awacs = await UnitRepository.FindAwacs(AwacsPilot, ServerShortName);
                if (awacs == null)
                {
                    // If you let the AWACS get shot down then don't expect useful responses
                    reply.Notes = $"{AwacsPilot} is the AWACS unit prefix and no unit with that prefix is alive on the server";
                    reply.Message = $"{GetResponseCallsign(transmission.Receiver)} is not on station";
                    reply.ContinueProcessing = false;
                    return reply;
                }
            }

            Unit playerUnit;
            (reply, playerUnit) = await DetermineTransmitter(transmission, reply, ServerShortName);

            if (reply.ContinueProcessing == false) return reply;

            switch (transmission.Intent)
            {
                case ITransmission.Intents.BogeyDope:
                    reply = await new BogeyDopeHandler(UnitRepository).Process(transmission, playerUnit, awacs, reply, ServerShortName, LosIsRestricted);
                    break;
                case ITransmission.Intents.RadioCheck:
                    reply.Message = "5 by 5";
                    break;
                case ITransmission.Intents.LocationOfEntity:
                    reply = await HandleLocationOfEntityTransmission(transmission, playerUnit, reply, ServerShortName);
                    reply.Notes = "Tankers not supported yet";
                    break;
                case ITransmission.Intents.SetWarningRadius:
                    reply = WarningRadiusHandler.Process(transmission, playerUnit, reply);
                    break;
                case ITransmission.Intents.CancelWarningRadius:
                    reply.Message = "copy, cancelling warnings";
                    break;
                case ITransmission.Intents.ReadyToTaxi:
                    reply.Message = AwacsOnlyMessage;
                    break;
                case ITransmission.Intents.InboundToAirfield:
                    reply.Message = AwacsOnlyMessage;
                    break;
                case ITransmission.Intents.MissionAssignment:
                    reply = await MissionAssignmentHandler.Process(transmission, playerUnit, reply, ServerShortName);
                    reply.Notes ??= "This only works on servers who have specifically implemented the feature";
                    break;
                case ITransmission.Intents.MissionJoin:
                    reply = await MissionJoinHandler.Process(transmission, playerUnit, reply, ServerShortName);
                    reply.Notes ??= "This only works on servers who have specifically implemented the feature";
                    break;
                case ITransmission.Intents.Declare:
                    reply.Message = "we are unable to support declare calls";
                    reply.Notes =
                        "Comment on https://forums.eagle.ru/topic/251461-request-for-scripting-apis-to-be-implemented-to-enhance-overlordbot-functionality if you want this";
                    break;
                case ITransmission.Intents.Picture:
                    reply.Message = "we do not support picture calls";
                    reply.Notes = "Difficult to implement well, prone to clogging radios, and low priority";
                    break;
                default:
                    _logger.Warn(
                        $"Unknown Intent: {transmission.Intent}, Transmitter: {transmission.Transmitter}, Receiver: {transmission.Receiver}");
                    return null;
            }
            return reply;
        }

        private static bool IsAddressedToAwacs(ITransmission.IReceiver receiver)
        {
            return receiver is ITransmission.Awacs;
        }

        private bool IsAddressedToController(ITransmission.IReceiver receiver)
        {
            if (receiver == null) return false;
            if (Callsign == null) return true;
            var awacs = (ITransmission.Awacs) receiver;
            // ReSharper disable once StringLiteralTypo
            return awacs.Callsign.ToLower() == Callsign || awacs.Callsign.ToLower() == "anyface";
        }

        private string GetResponseCallsign(ITransmission.IReceiver receiver)
        {
            // If the AWACS has a callsign set then use that
            if (Callsign != null) return Callsign;

            // If the called used "anyface" then respond with default callsign
            // Otherwise If the AWACS has no callsign set and the request was not "anyface" then use the request callsign
            return receiver.Callsign.ToLower() == "anyface" ? "overlord" : receiver.Callsign;
        }

        private async Task<Reply> HandleLocationOfEntityTransmission(ITransmission transmission, Unit transmitterUnit, Reply reply,
            string serverShortName)
        {
            var locationTransmission = (LocationOfEntityTransmission) transmission;
            switch (locationTransmission.Subject)
            {
                case ITransmission.Airfield _:
                    reply = await new LocationOfAirfieldHandler(AirfieldRepository).Process(locationTransmission, transmitterUnit, reply, serverShortName);
                    break;
                case ITransmission.Player _:
                    reply.Message = await new LocationOfUnitHandler(UnitRepository).Process(locationTransmission, transmitterUnit, serverShortName);
                    break;
                case ITransmission.Tanker _:
                    reply.Message = "we do not yet support vectors to tankers";
                    break;
                default:
                    reply.Message = "I could not determine what you are looking for";
                    break;
            }

            return reply;
        }
    }
}
