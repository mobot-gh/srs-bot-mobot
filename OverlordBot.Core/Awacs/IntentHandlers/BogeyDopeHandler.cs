﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Geo.Measure;
using NLog;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;
using RurouniJones.OverlordBot.Core.Responders;
using RurouniJones.OverlordBot.Core.Util;
using RurouniJones.OverlordBot.Datastore;
using RurouniJones.OverlordBot.Datastore.Models;
using RurouniJones.OverlordBot.Encyclopedia;

namespace RurouniJones.OverlordBot.Core.Awacs.IntentHandlers
{
    public class BogeyDopeHandler
    {
        public record BogeyDopeDetails : Reply.IReplyDetails
        {
            private readonly int _bearing;
            private readonly int _range; // In kilometers
            private readonly int _altitude; // In meters
            private readonly int _speed;
            private readonly string _aspect;
            private readonly List<Unit> _group;
            private readonly Reply.IReplyDetails.MeasurementSystem _system;

            public BogeyDopeDetails(int bearing, int range, int altitude, string aspect, int speed, List<Unit> group, Reply.IReplyDetails.MeasurementSystem system)
            {
                _bearing = bearing;
                _range = range;
                _altitude = altitude;
                _aspect = aspect;
                _speed = speed;
                _group = group;
                _system = system;
            }

            public string ToSpeech()
            {
                var sections = new List<string>
                {
                    "bra",
                    Reply.IReplyDetails.BearingToWords(_bearing),
                    Reply.IReplyDetails.RangeToWords(_range, _system),
                    Reply.IReplyDetails.AltitudeToWords(_altitude, _system),
                    _aspect
                };

                sections.AddRange(Reply.IReplyDetails.CommentsToWords(_altitude, _speed));
                sections.AddRange(Reply.IReplyDetails.UnitCountsToWords(_group));

                return string.Join(", ", sections);
            }

            public string ToText()
            {
                var distanceAcronym = _system == Reply.IReplyDetails.MeasurementSystem.Metric ? "km" : "nm";
                var altitudeAcronym = _system == Reply.IReplyDetails.MeasurementSystem.Metric ? "m" : "ft";


                int convertedAltitude;

                if (_system == Reply.IReplyDetails.MeasurementSystem.Metric)
                {
                    convertedAltitude = (int)Math.Round(_altitude / 100d, 0) * 100;
                }
                else
                {
                    convertedAltitude = (int)Math.Round(_altitude * 3.28d / 1000d, 0) * 1000;
                }

                if (convertedAltitude == 0) {
                    convertedAltitude = 500;
                }

                var sections = new List<string>
                {
                    $"BRA {_bearing.ToString("D3")}",
                    $"{Reply.IReplyDetails.RangeToWords(_range, _system)}{distanceAcronym}",
                    $"{convertedAltitude.ToString("N0")}{altitudeAcronym}",
                    _aspect
                };

                sections.AddRange(Reply.IReplyDetails.CommentsToWords(_altitude, _speed));
                sections.AddRange(Reply.IReplyDetails.UnitCountsToWords(_group));

                return string.Join(" | ", sections);
            }
        }

        private readonly IUnitRepository _unitRepository;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static readonly string[] CardinalPoints =
            {"NORTH", "NORTH EAST", "EAST", "SOUTH EAST", "SOUTH", "SOUTH WEST", "WEST", "NORTH WEST", "NORTH"};

        // If you are flying one of these you will be getting metric callouts. 
        public static readonly List<string> MetricAirframes = new()
        {
            "MiG-29A",
            "MiG-29S",
            "Su-27",
            "Su-33",
            "J-11A",
            "MiG-21Bis",
            "MiG-19P",
            "MiG-15",
            "MiG-15bis",
            "MiG-15Bis",
            "Su-25",
            "Su-25T",
            "Mi-8MT",
            "Ka-50",
            "Mi-24P",
            "L-39C",
            "L-39ZA",
            "AJS37",
            "SA342L",
            "SA342M",
            "SA342Minigun",
            "SA342Mistral",
        };

        public BogeyDopeHandler(IUnitRepository unitRepository)
        {
            _unitRepository = unitRepository;
        }

        public async Task<Reply> Process(ITransmission basicTransmission, Unit transmitterUnit, Unit awacsUnit, Reply reply, string serverShortName, bool losIsRestricted)
        {
            var transmission = (BogeyDopeTransmission) basicTransmission;

            List<string> aircraftCodes = null;
            var qualifier = (ITransmission.Aircraft) transmission.Qualifier;
            if (qualifier?.Identifier != null)
                aircraftCodes = Aircraft.GetCodesByIdentifier(qualifier.Identifier);
            else if (qualifier?.Attributes.Count > 0) aircraftCodes = Aircraft.GetCodesByAttributes(qualifier.Attributes);

            var contacts = await _unitRepository.FindHostileAircraft(transmitterUnit.Location, transmitterUnit.Coalition,
                aircraftCodes, serverShortName);

            Logger.Trace($"All Contacts:\n{LogContacts(transmitterUnit, contacts)}");
            // ReSharper disable once InvertIf
            if (awacsUnit != null && losIsRestricted)
            {
                contacts.RemoveAll(contact => !LineOfSightChecker.HasLineOfSight(awacsUnit, contact));
                Logger.Trace($"Visible Contacts:\n{LogContacts(transmitterUnit, contacts)}");
            }

            if (contacts.Count > 0)
            {
                reply.Details = BuildResponse(transmitterUnit, contacts);
            }
            else
            {
                reply.Message = "Nothing on Scope";
            }

            return reply;
        }

        public static BogeyDopeDetails BuildResponse(Unit transmitterUnit, IEnumerable<Unit> contacts)
        {
            contacts = contacts.ToList();
            var transmitterPoint = transmitterUnit.Location;
            var closestContact = contacts.First();

            Logger.Debug($"Closest {closestContact}");

            var trueBearing = transmitterUnit.BearingTo(closestContact);

            var bearing = (int) Geospatial.TrueToMagnetic(transmitterPoint, trueBearing);
            var range = (int)transmitterUnit.DistanceTo(closestContact, DistanceUnit.Km);
            var altitude = (int) closestContact.Altitude;
            var aspect = GetAspect((int) trueBearing, closestContact.Heading);

            // ReSharper disable once PossibleUnintendedReferenceComparison
            var group = contacts.Where(contact => closestContact != contact &&
                                                  closestContact.DistanceTo(contact, DistanceUnit.Nm) < 5 &&
                                                  DiffHeading(closestContact.Heading, contact.Heading) < 90 &&
                                                  Math.Abs(transmitterUnit.DistanceTo(closestContact, DistanceUnit.Nm) - transmitterUnit.DistanceTo(contact,
                                                      DistanceUnit.Nm)) < 3
            ).ToList();
            group.Add(closestContact);

            var system = MetricAirframes.Contains(transmitterUnit.Name)
                ? Reply.IReplyDetails.MeasurementSystem.Metric
                : Reply.IReplyDetails.MeasurementSystem.Imperial;

            return new BogeyDopeDetails(bearing, range, altitude, aspect, closestContact.Speed, group, system);
        }

        private static int DiffHeading(int a, int b)
        {
            return Math.Min(a - b < 0 ? a - b + 360 : a - b, b - a < 0 ? b - a + 360 : b - a);
        }

        /// <summary>
        ///     Return aspect information for a bogey dope according to ATP 3-52.4, Page 25
        /// </summary>
        /// <param name="bearing">Bearing from the friendly to the contact</param>
        /// <param name="heading">Heading of the contact</param>
        /// <returns></returns>
        private static string GetAspect(int bearing, int heading)
        {
            var angleAspect = DiffHeading(bearing, heading);

            if (heading < 0) heading += 360;

            var sb = new StringBuilder();

            switch (angleAspect)
            {
                case <= 65:
                    sb.Append("DRAG ");
                    sb.Append(CardinalPoints[(int) Math.Round((double) heading % 360 / 45)]);
                    break;
                case < 115:
                    sb.Append("BEAM ");
                    sb.Append(CardinalPoints[(int) Math.Round((double) heading % 360 / 45)]);
                    break;
                case < 155:
                    sb.Append("FLANK ");
                    sb.Append(CardinalPoints[(int) Math.Round((double) heading % 360 / 45)]);
                    break;
                default:
                    sb.Append("HOT");
                    break;
            }

            return sb.ToString();
        }

        private static string LogContacts(Unit player, IEnumerable<Unit> contacts)
        {
            var location = player.Location;
            var sb = new StringBuilder();
            foreach (var contact in contacts)
            {
                var bearing =
                    Regex.Replace(Geospatial.TrueToMagnetic(location, player.BearingTo(contact)).ToString("000"),
                        "\\d{1}", " $0");
                var range = (int) player.DistanceTo(contact, DistanceUnit.Nm);
                sb.AppendLine($"{bearing}/{range}: {contact.Id}, {contact.Name}, {contact.Group}");
            }

            return sb.ToString();
        }
    }
}
