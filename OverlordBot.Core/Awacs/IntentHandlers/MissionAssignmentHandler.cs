﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Threading.Tasks;
using Grpc.Net.Client;
using NLog;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;
using RurouniJones.OverlordBot.Core.Responders;
using RurouniJones.Dcs.Grpc.V0.Custom;
using Unit = RurouniJones.OverlordBot.Datastore.Models.Unit;

namespace RurouniJones.OverlordBot.Core.Awacs.IntentHandlers
{
    internal class MissionAssignmentHandler
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();


        public static async Task<Reply> Process(ITransmission basicTransmission, Unit transmitterUnit, Reply reply, string serverShortName)
        {
            var transmission = (MissionAssignmentTransmission) basicTransmission;

            if (transmission.MissionType == ITransmission.MissionType.Unknown)
            {
                reply.Message = "I did not hear the mission type";
                reply.ContinueProcessing = true;
                return reply;
            }

            try
            {
                var host = "localhost";
                var port = 50051;

                using var channel = GrpcChannel.ForAddress($"http://{host}:{port}");
                var client = new CustomService.CustomServiceClient(channel);
                await client.RequestMissionAssignmentAsync( new RequestMissionAssignmentRequest
                {
                    UnitName = transmitterUnit.Group,
                    MissionType = transmission.MissionType.ToString()
                });
            }
            catch (Exception e)
            {
                reply.Message = "unable to forward request";
                reply.ContinueProcessing = false;
                Logger.Error(e);
                return reply;
            }

            reply.Message = "copy, request forwarded";
            reply.ContinueProcessing = true;
            return reply;
        }
    }
}
