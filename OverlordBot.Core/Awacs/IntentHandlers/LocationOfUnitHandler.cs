﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Geo.Measure;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;
using RurouniJones.OverlordBot.Core.Util;
using RurouniJones.OverlordBot.Datastore;
using RurouniJones.OverlordBot.Datastore.Models;

namespace RurouniJones.OverlordBot.Core.Awacs.IntentHandlers
{
    internal class LocationOfUnitHandler
    {
        private readonly IUnitRepository _unitRepository;

        public LocationOfUnitHandler(IUnitRepository unitRepository)
        {
            _unitRepository = unitRepository;
        }

        public async Task<string> Process(LocationOfEntityTransmission transmission, Unit transmitterUnit, string serverShortName)
        {
            var subjectPlayer = (ITransmission.Player) transmission.Subject;

            var subjectUnit = await _unitRepository.FindByCallsignAndCoalition(subjectPlayer.GroupName, subjectPlayer.Flight,
                subjectPlayer.Element, transmitterUnit.Coalition);

            return subjectUnit == null
                ? $"I could not find {subjectPlayer.SpokenWithAmbiguity()}"
                : BuildResponse(transmitterUnit, subjectUnit);
        }

        private static string BuildResponse(Unit transmitterUnit, Unit subjectUnit)
        {
            var transmitterPoint = transmitterUnit.Location;

            var bearing =
                Regex.Replace(
                    Geospatial.TrueToMagnetic(transmitterPoint, transmitterUnit.BearingTo(subjectUnit)).ToString("000"),
                    "\\d{1}", " $0")[1..];

            int range;
            int altitude;
            if (BogeyDopeHandler.MetricAirframes.Contains(transmitterUnit.Name))
            {
                range = (int) transmitterUnit.DistanceTo(subjectUnit, DistanceUnit.Km);
                altitude = (int) Math.Round(subjectUnit.Altitude / 100d, 0) * 100; // Round to the nearest 100 meters 
            }
            else
            {
                range = (int)transmitterUnit.DistanceTo(subjectUnit, DistanceUnit.Nm);
                altitude = (int) (Math.Round(subjectUnit.Altitude * 3.28d / 1000d, 0) * 1000); // Convert meters to feet and round to nearest thousand
            }

            double angels;
            if (altitude < 1000)
                angels = 1;
            else
                // ReSharper disable once PossibleLossOfFraction
                angels = (altitude % 1000 >= 500 ? altitude + 1000 - altitude % 1000 : altitude - altitude % 1000) /
                         1000;

            return $"Bra, {bearing}, {range}, Angels {angels}";
        }
    }
}