﻿# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/dotNET-Core.yml

# ### Specify the Docker image
#
# Instead of installing .NET Core SDK manually, a docker image is used
# with already pre-installed .NET Core SDK.
#
# The 'latest' tag targets the latest available version of .NET Core SDK image.
#
# See other available tags for .NET Core: https://hub.docker.com/_/microsoft-dotnet
# Learn more about Docker tags: https://docs.docker.com/glossary/?term=tag
# and the Docker itself: https://opensource.com/resources/what-docker
image: mcr.microsoft.com/dotnet/sdk:6.0

# ### Define variables
variables:
  # 1) Name of directory where restore and build objects are stored.
  OBJECTS_DIRECTORY: 'obj'
  # 2) Name of directory used for keeping restored dependencies.
  NUGET_PACKAGES_DIRECTORY: '.nuget'
  # 3) A relative path to the source code from project repository root.
  # NOTE: Please edit this path so it matches the structure of your project!
  SOURCE_CODE_PATH: '*/*/'

# ### Define global cache rule
#
# Before building the project, all dependencies (e.g. third-party NuGet packages)
# must be restored. Jobs on GitLab.com's Shared Runners are executed on autoscaled machines.
#
# Each machine is used only once (for security reasons) and after that is removed.
# This means that, before every job, a dependency restore must be performed
# because restored dependencies are removed along with machines. Fortunately,
# GitLab provides cache mechanism with the aim of keeping restored dependencies
# for other jobs.
#
# This example shows how to configure cache to pass over restored
# dependencies for re-use.
#
# With global cache rule, cached dependencies will be downloaded before every job
# and then unpacked to the paths as specified below.
cache:
  # Per-stage and per-branch caching.
  key: "$CI_JOB_STAGE-$CI_COMMIT_REF_SLUG"
  paths:
    # Specify three paths that should be cached:
    #
    # 1) Main JSON file holding information about package dependency tree, packages versions,
    # frameworks etc. It also holds information where to the dependencies were restored.
    - '$SOURCE_CODE_PATH$OBJECTS_DIRECTORY/project.assets.json'
    # 2) Other NuGet and MSBuild related files. Also needed.
    - '$SOURCE_CODE_PATH$OBJECTS_DIRECTORY/*.csproj.nuget.*'
    # 3) Path to the directory where restored dependencies are kept.
    - '$NUGET_PACKAGES_DIRECTORY'

before_script:
  - 'dotnet restore --packages $NUGET_PACKAGES_DIRECTORY'

stages:
  - build
  - test
  - release

build:
  stage: build
  script:
    - 'dotnet build --no-restore'

tests:
  stage: test
  script:
    - 'dotnet test --no-restore'

release:
  stage: release
  only:
    - tags
  parallel:
    matrix:
      - RUNTIME_ID: [win-x64, linux-x64]
  artifacts:
    paths:
      - OverlordBot-$CI_BUILD_TAG-$RUNTIME_ID.zip
  script:
    - apt-get update
    - apt-get install -y --no-install-recommends zip
    - dotnet publish -c Release -o publish --self-contained true -r $RUNTIME_ID OverlordBot.Console/OverlordBot.Console.csproj
    - cd publish/
    - zip -r ../OverlordBot-$CI_BUILD_TAG-$RUNTIME_ID.zip .
